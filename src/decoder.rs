use log;
use openh264_sys as ffi;
use std::ptr;

use crate::params::DecodingParams;

pub struct DecoderBuilder {
    params: DecodingParams,
}

#[derive(Debug)]
pub struct Decoder {
    inner: ffi::ISVCDecoder,
    params: DecodingParams,
}

impl DecoderBuilder {
    pub fn new() -> Self {
        DecoderBuilder {
            params: DecodingParams::default(),
        }
    }

    pub fn build(self) -> Decoder {
        let mut dec = ptr::null_mut();
        unsafe {
            if ffi::WelsCreateDecoder(&mut dec) != 0 {
                panic!("cannot create Decoder");
            };
            if (**dec).Initialize.unwrap()(dec, self.params.as_ref()) != 0 {
                panic!("Cannot build decoder");
            };
        }
        Decoder {
            inner: dec as *const _,
            params: self.params,
        }
    }
}

impl Drop for Decoder {
    fn drop(&mut self) {
        unsafe {
            (**(self.inner as *mut *const ffi::ISVCDecoderVtbl))
                .Uninitialize
                .unwrap()(self.inner as *mut *const _);
            ffi::WelsDestroyDecoder(self.inner as *mut *const _);
        }
        log::trace!("Decoder dropped")
    }
}
