use openh264_sys as ffi;
use std::convert::AsRef;

#[derive(Default, Debug, Copy, Clone)]
pub struct DecodingParams {
    inner: ffi::SDecodingParam,
}

impl AsRef<ffi::SDecodingParam> for DecodingParams {
    fn as_ref(&self) -> &ffi::SDecodingParam {
        &self.inner
    }
}
