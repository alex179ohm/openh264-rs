pub mod decoder;
pub mod params;

#[cfg(test)]
mod tests {
    use super::decoder::DecoderBuilder;

    #[test]
    fn decoder_new_test() {
        let _dec = DecoderBuilder::new().build();
    }
}
